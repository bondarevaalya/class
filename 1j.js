const name = prompt("What is your name?");
let age = null

do {
    age = +prompt('What is your age?');
} while (!age || Number.isNaN(age));

if (age < 18) {
    alert('You are not allowed to visit this website');
}  else if (age >= 18 && age <= 22) {
    result = confirm('Are you sure you want to continue?');
    if (result) {
        alert(`Welcome, ${name}!`);
    } else {
        alert('You are not allowed to visit this website');
    }
}else {
    alert(`Welcome, ${name}!`);
}
// переменные, объявлены при помощи var, могут стартовать как undefined, a let такого не позволяет.
// переменные, которые объявлены с помощью const и let, доступны только в рамках того блока, внутри  которого они объявлены. переменная объявлена через var видна везде в функции
//
// var не может быть блочной или локальной внутри функции, та же var игнорирует блоки
