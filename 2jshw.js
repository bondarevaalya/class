let number = null

do {
    number = +prompt('write your number');
} while (!number || Number.isNaN(number));

for (let i = 1; i <= number; i++) {
    if (i % 5 === 0) {
        console.log(i)
    } else if (number < 5){
        alert('Sorry, no numbers')
    }
}

//LOOPS (циклы) нужны для того, что б выполнять действие одно и тоже,
// большое количество раз и не писать каждый раз одну и ту же строчку.
// тем самым можно обернуть все в один цикл